swift-tools (0.0.24) unstable; urgency=medium

  [ Philippe SERAPHIN ]
  * [4029564] Add handoff replication jobs in st-send-replication-progress
  * [c56e2b8] Add async_pending in send_replication_progress
  * [d00bb51] Sort filling disk by value
  * [b8032be] Change logfile path
  * [68e28f9] Change logfile path
  * [9d6a69f] Conform to flake8
  * [dc08e57] Add choice of server type on st-check-replication
  * [d4097e5] Correct check_dark_date.py 
              Add display of repartition of IN and OUT partitions between two rings
  * [e5fcbf2] Bug correction
  * [17a6b35] Add st-check-dark-data
  * [f620681] Coorect bug in compare_ring.py when openstack swift version is too old
  * Add st-check-rsync-disable
  * Suppress numpy dependancy

  [ Thomas Goirand ]
  * Switch to pybuild (Closes: #1090699).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 14:34:34 +0100

swift-tools (0.0.8) unstable; urgency=medium

  [ Philippe SERAPHIN ]
  * [003cfbe] Add st-send-replication-progress
  * [1c20d2b] Add random pause before send data
  * [5a2e82c] Correct bug when log file is empty

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Jan 2024 09:20:22 +0100

swift-tools (0.0.6) unstable; urgency=medium

  [ Philippe SERAPHIN ]
  * Add st-status 
  * Correction minor bug
  * Correct typo
  * Change setup.cfg
  * Add bash-completion for st-check-oldest-completion.
  * Add manpages for st-check-replication and st-check-oldest-completion.
  * Add manpage for st-status Add bash-completion for st-status
  * - Correction error in completions files

  [ Thomas Goirand ]
  * Package st-check-oldest-completion.1.
  * Fix setup.cfg.
  * Remove usless stuff in setup.py.
  * Add if __name__ == "__main__": sys.exit(main()) in all scripts, so they can
    also be launch directly.

 -- Philippe Seraphin <philippe.seraphin@infomaniak.com>  Thu, 18 Jan 2024 09:10:34 +0100

swift-tools (0.0.1) unstable; urgency=medium

  * Initial release. (Closes: #1057636)

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Nov 2023 11:54:32 +0100
